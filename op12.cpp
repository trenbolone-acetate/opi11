﻿#include <iostream> 
#include "stdio.h"
#include "windows.h"
#include <string>

using namespace std;

/*
    Створити масив структур, який містить відомості про книжковий магазин. 
    Кожна структура містить поля: назва книги, кількість примірників, ціна. 
    Реалізувати виведення всієї інформації про книги, пошук книги за назвою.
    Реалізувати пошук за ціною із зазначенням інтервалу можливої ціни 
    (повинна бути виведена інформація про книги, ціни яких потрапляють у зазначений інтервал).
*/



int main()
{
    SetConsoleCP(1251);
    SetConsoleTitleA("ОП 12");
    struct book {
        char title[20];
        int amount;
        float price;
    };
    string choice;

    book Array[3] = { { "It", 14, 69.99 },
                    { "Idiot", 6, 249.99 },
                    { "The Viy", 10, 199.99 } };

    cout << " Search by name (1) or by price range(2): ";
    cin >> choice;
    if (choice == "1") {
        string search;
        cout << endl << "Search by name: ";
        cin.ignore();
        getline(cin, search);

        for (int i = 0; i < 3; i++)
        {
            if (search == Array[i].title) {
                cout << endl << "The name of the book is " << Array[i].title << endl << "The amount of books in the store " << Array[i].amount << endl << "The book costs " << Array[i].price << endl;
            }
        }
    }
    else if (choice == "2") {
        double from, to;
        cout << "Enter the price range \nFrom: ";
        cin >> from;
        cout << "To: ";
        cin >> to;

        for (int i = 0; i < 3; i++)
        {
            if (Array[i].price > from && Array[i].price < to ) {
                cout << endl << "The book with suitable price is " << Array[i].title << endl << "The amount of books in the store " << Array[i].amount << endl << "The book costs " << Array[i].price << endl;
            }
           
        }
    }
    else {
        cout << "Error, you need to type 1 or 2";
    }
    return 0;
}

